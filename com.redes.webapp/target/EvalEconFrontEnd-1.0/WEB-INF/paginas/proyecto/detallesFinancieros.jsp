<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="mx-5">
    <h4 class="my-3">Detalles financieros</h4>
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Inversi�n inicial
                    </button>
                </h2>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <form class="needs-validation" novalidate action="${pageContext.request.contextPath}/ServletControlador?accion=agregarActivo" method="POST">
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Nombre del activo</label>
                                <input name="txtnombreAct" type="text" class="form-control" id="validationCustom01" placeholder="Nombre del activo" value="" required>
                                <div class="invalid-feedback">
                                    Favor de inidicar nombre de activo
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Ubicaci�n</label>
                                <input name="txtUbicacionAct" type="text" class="form-control" id="validationCustom02" placeholder="Ubicaci�n" value="" required>
                                <div class="invalid-feedback">
                                    Favor de inidicar ubicaci�n de activo
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Tipo de activo</label>
                                <div class="input-group">
                                    <div class="form-group">
                                        <select name="cboTipoAct" class="custom-select" required>
                                            <option value="">Seleccionar...</option>
                                            <option value="Activo fijo">Activo fijo</option>
                                            <option value="Activo diferido">Activo diferido</option>
                                        </select>
                                        <div class="invalid-feedback">Favor de seleccionar opci�n v�lida</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationCustom03">Cantidad</label>
                                <input name="txtCantidadAct" type="number" class="form-control" id="validationCustom03" placeholder="Cantidad del activo" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                                <div class="invalid-feedback">
                                    Favor de indicar una cantidad
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustom04">Valor del activo</label>
                                <input name="txtUnitarioAct" type="number" class="form-control" id="validationCustom04" placeholder="Valor del activo" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                                <div class="invalid-feedback">
                                    Favor de indicar valor unitario
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Agregar activo</button>
                    </form>
                    <div class="text-center mt-3">
                        <h5>Listado de activos</h5>
                    </div>

                    <table class="table table-striped" id="listadoActivos">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col"Nombre activo></th>
                                <th scope="col">Ubicaci�n</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Valor unitario</th>
                                <th scope="col">Valor total</th>
                                <th scope="col">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="act" items="${listadoActivos}">
                                <tr>
                                    <td>${act.idActivo}</td>
                                    <td>${act.nombreActivo}</td>
                                    <td>${act.areaUbicacion}</td>
                                    <td>${act.tipoActivo}</td>
                                    <td>${act.cantidad}</td>
                                    <td><fmt:formatNumber value="${act.valorUnitarioAct}" type="currency" /></td>
                                    <td><fmt:formatNumber value="${act.valorTotalAct}" type="currency" /></td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/ServletControlador?accion=editar&idProyecto=${pr.idProyecto}"
                                           class="btn btn-warning">
                                            <i class="fas fa-pen-to-square"></i> Editar
                                        </a>
                                        <a href="${pageContext.request.contextPath}/ServletControlador?accion=editar&idProyecto=${pr.idProyecto}"
                                           class="btn btn-danger">
                                            <i class="fas fa-trash"></i> Eliminar
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                    <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function () {
                            'use strict';
                            window.addEventListener('load', function () {
                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                var forms = document.getElementsByClassName('needs-validation');
                                // Loop over them and prevent submission
                                var validation = Array.prototype.filter.call(forms, function (form) {
                                    form.addEventListener('submit', function (event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                    </script>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Ventas presupuestadas
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    PRESUPUSTO DE VENTAS
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Costos presupuestados
                    </button>
                </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="mo-tab" data-toggle="tab" href="#mo" role="tab" aria-controls="mo" aria-selected="true">Mano de obra</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="mp-tab" data-toggle="tab" href="#mp" role="tab" aria-controls="mp" aria-selected="false">Materia prima</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="insumos-tab" data-toggle="tab" href="#insumos" role="tab" aria-controls="insumos" aria-selected="false">Insumos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="servicios-tab" data-toggle="tab" href="#servicios" role="tab" aria-controls="servicios" aria-selected="false">Servicios</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="mo" role="tabpanel" aria-labelledby="mo-tab">MESERO TAQUERO MESERO TAQUERO LAVALOZA</div>
                        <div class="tab-pane fade" id="mp" role="tabpanel" aria-labelledby="mp-tab">TORTILLAS CARNE SALSA </div>
                        <div class="tab-pane fade" id="insumos" role="tabpanel" aria-labelledby="insumos-tab">VASOS ART LIMPIEZA UNICELES</div>
                        <div class="tab-pane fade" id="servicios" role="tabpanel" aria-labelledby="servicios-tab">LUZ AGUA TELEFONO RENTA</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFour">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Flujos Netos de Efectivo
                    </button>
                </h2>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                    Estado de resultados pro forma
                </div>
            </div>
        </div>
    </div>
</div>