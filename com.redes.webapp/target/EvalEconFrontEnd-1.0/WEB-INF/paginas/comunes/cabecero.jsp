<header id="main-header" class="py-2 bg-secondary text-white">
    <div class="container mx-3">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    <i class="fas fa-cog"></i> Control de Proyectos</h1>
            </div>
        </div>
        <div class="row d-flex justify-content-start">
            <div class="col-md-1">
                <label>ID. ${usuActual.idUsuario}</label> &nbsp;&nbsp;
            </div>
            <div class="col-md-4">
                <label>Usuario. ${usuActual.nombre} ${usuActual.paterno} ${usuActual.materno}</label>
            </div>
            <div class="d-flex justify-content-end">
                <div class="col-md-4">
                    <a href="ServletControlador?accion=salir" class="btn" href="#">Cerrar Sesi�n</a>
                </div>
            </div>
        </div>
    </div>
</header>