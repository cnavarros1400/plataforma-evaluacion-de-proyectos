<section id="actions" class="py-4 mb-4 bg-light">
    <div class="container mx-3">
        <div class="row">
            <div class="col-md-3">
                <a href="proyectos.jsp" class="btn btn-ligth btn-block">
                    <i class="fas fa-arrow-left"></i> Regresar al inicio
                </a>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-success btn-block">
                    <i class="fas fa-check"></i> Guardar y evaluar
                </button>
            </div>
            <div class="col-md-3">
                <a href="${pageContext.request.contextPath}/ServletControlador?accion=eliminar&idCliente=${cliente.idCliente}"
                   class="btn btn-primary btn-block">
                    <i class="fas fa-file-export"></i> Exportar proyecto
                </a>
            </div>
            <div class="col-md-3">
                <a href="${pageContext.request.contextPath}/ServletControlador?accion=eliminar&idCliente=${cliente.idCliente}"
                   class="btn btn-danger btn-block">
                    <i class="fas fa-trash"></i> Eliminar proyecto
                </a>
            </div>

        </div>
    </div>
</section>