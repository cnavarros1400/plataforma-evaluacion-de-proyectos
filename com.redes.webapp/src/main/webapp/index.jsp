<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--<meta http-equiv="refresh" content="0;url=${pageContext.request.contextPath}/ServletControlador">-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <title>Plataforma de evaluación económica</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <form action="ServletControlador" method="POST" class="was-validated">
                        <div class="form-group">
                            <div class="text-center"><h1>Ingresar a plataforma</h1></div>
                        </div>
                        <div class="text-center">
                            <img src="img/iconoUsuarioLogin.png" height="100" width="100"/>
                        </div>
                        <div class="form-group">
                            <label>Correo. </label>
                            <input class="form-control" type="text" name="txtCorreo" placeholder="Ingresar correo" required>
                        </div>
                        <div class="form-group">
                            <label>Contraseña. </label>
                            <input class="form-control" type="password" name="txtContrasena" placeholder="Ingresar contraseña" required>
                        </div>
                        <div class="text-center">
                            <input class="btn btn-primary btn-block" type="submit" name="accion" value="ingresar" required>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3">
                    <form action="ServletControlador" method="POST" class="was-validated">
                        <div class="form-group">
                            <div class="text-center"><h1>Registrarme en plataforma</h1></div>
                        </div>
                        <div class="text-center">
                            <img src="img/iconoUsuarioLogin.png" height="100" width="100"/>
                        </div>
                        <div class="form-group">
                            <label>Nombre(s). </label>
                            <input class="form-control" type="text" name="txtNombre" placeholder="Ingresar nombre(s)" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido paterno. </label>
                            <input class="form-control" type="text" name="txtPaterno" placeholder="Ingresar apellido paterno" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido materno. </label>
                            <input class="form-control" type="text" name="txtMaterno" placeholder="Ingresar apellido materno" required>
                        </div>
                        <div class="form-group">
                            <label>Correo. </label>
                            <input class="form-control" type="text" name="txtCorreoReg" placeholder="Ingresar correo" required>
                        </div>
                        <div class="form-group">
                            <label>Contraseña. </label>
                            <input class="form-control" type="password" name="txtContrasenaReg" placeholder="Ingresar contraseña" required>
                        </div>
                        <div class="text-center">
                            <input class="btn btn-primary btn-block" type="submit" name="accion" value="registrar">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    </body>
</html>
