<div class="container">
    <section id="actions" class="py-4 mb-4 mx-3 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a href="#" class="btn btn-primary btn-block"
                       data-toggle="modal" data-target="#agregarProyectoModal">
                        <i class="fas fa-plus"></i> Agregar proyecto
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>
