<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container mx-3 ">

    <h4 class="my-2">Resumen del proyecto</h4>
    <div class="row d-flex justify-content-start my-2">
        <div class="col-sm-1">
            ID. ${proyecto.idProyecto}
        </div>
        <div class="col-sm-3">
            Proyecto. ${proyecto.nombreProyecto}
        </div>
        <div class="col-sm-8">
            Fecha de creaci�n. <fmt:formatDate value="${proyecto.fecha}" />
        </div>
    </div>
    <div class="row d-flex justify-content-start my-2">
        <div class="col d-inline-flex p-2 bd-highlight">
            Inversi�n inicial. <br> <fmt:formatNumber value="${proyecto.inversionInicial}" type="currency"/>
        </div>
        <div class="col d-inline-flex p-2 bd-highlight">
            Ventas presupuestadas. <br> <fmt:formatNumber value="${proyecto.presupuestoVentas}" type="currency" />
        </div>
        <div class="col">
            Costos presupuestados. <br> <fmt:formatNumber value="${proyecto.presupuestoCostos}" type="currency" />
        </div>
        <div class="col">
            FNE inicial. <br> <fmt:formatNumber value="${proyecto.flujoNetoEfectivoInicial}" type="currency" />
        </div>
        <div class="col">
            <c:choose>
                <c:when test="${proyecto.proyectoViable eq 1}">
                    Viabilidad. <br> Proyecto viable
                </c:when>
                <c:when test="${proyecto.proyectoViable eq 0}">
                    Viabilidad. <br> Proyecto no viable
                </c:when>
                <c:otherwise>Viabilidad. <br> No definida</c:otherwise>
            </c:choose>
        </div>
    </div>
</div>