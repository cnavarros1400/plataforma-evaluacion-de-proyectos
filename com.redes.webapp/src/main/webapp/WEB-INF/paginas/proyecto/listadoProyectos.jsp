<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="es_MX"/>

<section id="clientes">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                        <h4>Tus proyectos</h4>
                        <label></label>
                        </div>
                        
                    </div>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">T�tulo de proyecto</th>
                                <th scope="col">Fecha de creaci�n</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Iteramos cada elemento de la lista de clientes -->
                            <c:forEach var="pr" items="${proyectos}" >
                                <tr>
                                    <td>${pr.idProyecto}</td>
                                    <td>${pr.nombreProyecto}</td>
                                    <td> <fmt:formatDate value="${pr.fecha}"/> </td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/ServletControlador?accion=editar&idProyecto=${pr.idProyecto}"
                                           class="btn btn-secondary">
                                            <i class="fas fa-angle-double-right"></i> Abrir
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- Agregar cliente MODAL -->
<jsp:include page="/WEB-INF/paginas/proyecto/agregarProyecto.jsp"/>
