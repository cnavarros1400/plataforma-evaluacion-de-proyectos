package web;

import datos.ProyectoDao;
import datos.UsuarioDao;
import dominio.Proyecto;
import dominio.Usuario;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {

    @Inject
    UsuarioDao usuDao;
    @Inject
    ProyectoDao proDao;
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "editar":
                    this.editarProyecto(request, response);
                    break;
                case "eliminar":
                    this.eliminarCliente(request, response);
                    break;
                case "salir":
                    //request.getRequestDispatcher("index.jsp").forward(request, response);
                    System.out.println("CERRANDO SESION");
                    request.getSession().removeAttribute("usuActual");
                    request.getSession().removeAttribute("proyectos");
                    if (request.getSession().getAttribute("usuActual") == null) {
                        System.out.println("usuActual removido");
                    }
                    if (request.getSession().getAttribute("proyectos") == null) {
                        System.out.println("proyectos removidos");
                    }
                    request.getSession().invalidate();
                    response.sendRedirect("index.jsp");
                    break;
                default:
                    this.accionDefault(request, response);
            }
        } else {
            this.accionDefault(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "insertar":
                    this.insertarProyecto(request, response);
                    break;
                case "modificar":
                    this.modificarCliente(request, response);
                    break;
                case "ingresar":
                    this.accionIngresar(request, response);
                    break;
                case "registrar":
                    this.accionRegistrar(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
            }
        } else {
            this.accionDefault(request, response);
        }
    }

    private void accionDefault(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession sesion = request.getSession();
        Usuario usuActual = (Usuario) sesion.getAttribute("usuActual");
        Usuario actualizadoUsu = usuDao.encontrarUsuario(usuActual);
        List<Proyecto> proyectos = actualizadoUsu.getProyectoList();
        Iterator it = proyectos.iterator();
        while (it.hasNext()) {
            System.out.println("Proyectos asociados: " + it.next().toString());
        }
        request.getSession().setAttribute("usuActual", actualizadoUsu);
        request.getSession().setAttribute("proyectos", proyectos);
        //request.getSession().setAttribute("totalProyectos", proyectos.size());

        //request.getRequestDispatcher("proyectos.jsp").forward(request, response);
        response.sendRedirect("proyectos.jsp");
    }

    private void accionIngresar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Usuario usuario = new Usuario();
        Usuario acceso;
        HttpSession sesion = request.getSession();
        String correo = request.getParameter("txtCorreo");
        String contrasena = request.getParameter("txtContrasena");
        usuario.setCorreo(correo);
        usuario.setContrasena(contrasena);
        acceso = usuDao.encontrarUsuario(usuario);
        if (acceso != null) {
            System.out.println("Usuario logeado: " + acceso);
            List<Proyecto> proyectos = acceso.getProyectoList();
            Iterator it = proyectos.iterator();
            while (it.hasNext()) {
                System.out.println("Proyectos asociados: " + it.next().toString());
            }
            sesion.setAttribute("proyectos", proyectos);
            sesion.setAttribute("usuActual", acceso);
            //this.accionDefault(request, response);
            response.sendRedirect("proyectos.jsp");
        } else {
            System.out.println("Error de acceso: Credenciales incorrectas");
            response.sendRedirect("index.jsp");
            //request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    private void accionRegistrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Usuario usuarioReg = new Usuario();
        String nombre = request.getParameter("txtNombre");
        String aPaterno = request.getParameter("txtPaterno");
        String aMaterno = request.getParameter("txtMaterno");
        String correo = request.getParameter("txtCorreoReg");
        String pass = request.getParameter("txtContrasenaReg");
        usuarioReg.setNombre(nombre);
        usuarioReg.setPaterno(aPaterno);
        usuarioReg.setMaterno(aMaterno);
        usuarioReg.setCorreo(correo);
        usuarioReg.setContrasena(pass);
        usuDao.crearUsuario(usuarioReg);
        System.out.println("Usuario registrado. " + usuarioReg);
        String msg = "registroOk";
        request.getSession().setAttribute("aviso", msg);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    private void editarProyecto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //recuperamos el idProyecto
        int idProyecto = Integer.parseInt(request.getParameter("idProyecto"));
        Proyecto pr = new Proyecto(idProyecto);
        Proyecto proyecto = proDao.encontrarProyecto(pr);
        request.getSession().setAttribute("proyecto", proyecto);
        String jspEditar = "/WEB-INF/paginas/proyecto/editarProyecto.jsp";
        request.getRequestDispatcher(jspEditar).forward(request, response);
    }

    private void insertarProyecto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //recuperamos los valores del formulario agregarProyecto
        String nombre = request.getParameter("txtNombreProyecto");

        Usuario usuActual = (Usuario) request.getSession().getAttribute("usuActual");
        Date fecha = new Date();
        //Creamos el objeto de proyecto (modelo)
        Proyecto proyecto = new Proyecto(usuActual, nombre, fecha);

        //Insertamos el nuevo objeto en la base de datos
        proDao.crearProyecto(proyecto);
        System.out.println("Registro agregado: " + proyecto);

        //Redirigimos hacia accion por default
        //this.accionDefault(request, response);
        response.sendRedirect("proyectos.jsp");
    }

    private void modificarCliente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //recuperamos los valores del formulario editarCliente
        int idCliente = Integer.parseInt(request.getParameter("idCliente"));
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String email = request.getParameter("email");
        String telefono = request.getParameter("telefono");
        double saldo = 0;
        String saldoString = request.getParameter("saldo");
        if (saldoString != null && !"".equals(saldoString)) {
            saldo = Double.parseDouble(saldoString);
        }

        //Creamos el objeto de cliente (modelo)
        Proyecto proyecto = new Proyecto();

        //Modificar el  objeto en la base de datos
        proDao.modificarProyecto(proyecto);
        System.out.println("Modificacion: " + proyecto);

        //Redirigimos hacia accion por default
        this.accionDefault(request, response);
    }

    private void eliminarCliente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //recuperamos los valores del formulario editarCliente
        int idProyecto = Integer.parseInt(request.getParameter("idProyecto"));

        //Creamos el objeto de cliente (modelo)
        Proyecto proyecto = new Proyecto(idProyecto);

        //Eliminamos el  objeto en la base de datos
        proDao.eliminarProyecto(proyecto);
        System.out.println("registro eliminado con id = " + idProyecto);

        //Redirigimos hacia accion por default
        this.accionDefault(request, response);
    }


}
