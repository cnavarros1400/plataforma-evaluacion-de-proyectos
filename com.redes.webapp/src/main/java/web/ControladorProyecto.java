/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import datos.ActivoDao;
import datos.ProyectoDao;
import dominio.Activo;
import dominio.Proyecto;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MarkII
 */
@WebServlet("/ControladorProyecto")
public class ControladorProyecto extends HttpServlet{
    @Inject
    ActivoDao actDao;
    @Inject
    ProyectoDao prDao;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminarActivo":
                    this.eliminarActivo(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
            }
        } else {
            this.accionDefault(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "agregarActivo":
                    this.agregarActivo(request, response);
                    break;
                case "modificarActivo":
                    this.modificarActivo(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
            }
        } else {
            this.accionDefault(request, response);
        }
    }
    
    private void accionDefault(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Proyecto proyecto = (Proyecto) request.getSession().getAttribute("proyecto");
        Proyecto proActual = prDao.encontrarProyecto(proyecto);
        List<Activo> listadoActivos = proActual.getActivoList();
        request.getSession().setAttribute("listadoActivos", listadoActivos);
        request.getSession().setAttribute("proyecto", proyecto);
        response.sendRedirect("editarProyecto");
    }
    
    private void agregarActivo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //recuperamos los valores del formulario agregarActivo
        //int idCliente = Integer.parseInt(request.getParameter("idCliente"));
        String nombre = request.getParameter("txtnombreAct");
        String ubicacion = request.getParameter("txtUbicacionAct");
        String tipo = request.getParameter("cboTipoAct");
        int cantidad = Integer.parseInt(request.getParameter("txtCantidadAct"));
        double unitario = Double.parseDouble(request.getParameter("txtUnitarioAct"));
        //calculando total activo
        double totalActivo = cantidad * unitario;
        //Recuperamos proyecto
        Proyecto proyecto = (Proyecto) request.getSession().getAttribute("proyecto");
        //creacion del modelo
        Activo nuevoActivo = new Activo(nombre, ubicacion, tipo, cantidad, unitario, totalActivo, proyecto);
        //persistencia
        actDao.crearActivo(nuevoActivo);
        System.out.println("Registro agregado: " + nuevoActivo);
        this.accionDefault(request, response);
    }

    private void modificarActivo(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void eliminarActivo(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
