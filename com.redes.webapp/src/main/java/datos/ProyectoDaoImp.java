/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Proyecto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MarkII
 */
@Stateless
public class ProyectoDaoImp implements ProyectoDao{
    @PersistenceContext(unitName = "EvalEconPU")
    EntityManager em;

    @Override
    public Proyecto encontrarProyecto(Proyecto proyecto) {
        return em.find(Proyecto.class, proyecto.getIdProyecto());
    }
    
    @Override
    public void crearProyecto(Proyecto proyecto){
        em.persist(proyecto);
        em.flush();
    }
    
    @Override
    public void modificarProyecto(Proyecto proyecto){
        em.merge(proyecto);
    }
    
    @Override
    public void eliminarProyecto(Proyecto proyecto){
        em.remove(em.merge(proyecto));
    }
    
}
