package datos;

import dominio.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class UsuarioDaoImp implements UsuarioDao{

    @PersistenceContext(unitName = "EvalEconPU")
    EntityManager em;

    @Override
    public Usuario encontrarUsuario(Usuario usuario) {
        Usuario usuarioEncontrado;
        try {
            Query query = em.createQuery("SELECT u FROM Usuario u WHERE u.correo = :correo");
            query.setParameter("correo", usuario.getCorreo());
            usuarioEncontrado = (Usuario) query.getSingleResult();

        } catch (NoResultException e) {
            usuarioEncontrado = null;
        }

        if (usuarioEncontrado != null) {
            if (usuario.getContrasena().equals(usuarioEncontrado.getContrasena())) {
                return usuarioEncontrado;
            } else {
                usuarioEncontrado = null;
                return usuarioEncontrado;
            }
        } else {
            usuarioEncontrado = null;
            return usuarioEncontrado;
        }
    }

    @Override
    public void crearUsuario(Usuario usuario) {
        em.persist(usuario);
        //em.flush();
    }

  

}
