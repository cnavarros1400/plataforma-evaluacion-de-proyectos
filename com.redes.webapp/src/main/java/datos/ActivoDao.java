/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Activo;
import java.util.List;

/**
 *
 * @author MarkII
 */
public interface ActivoDao {
    public Activo encontrarActivo(Activo activo);
    public List<Activo> listarActivos();
    public void crearActivo(Activo activo);
    public void modificarActivo(Activo activo);
    public void eliminarActivo(Activo activo);
}
