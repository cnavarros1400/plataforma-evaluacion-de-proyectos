/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Proyecto;

/**
 *
 * @author MarkII
 */
public interface ProyectoDao {
    public Proyecto encontrarProyecto(Proyecto proyecto);
    public void crearProyecto(Proyecto proyecto);
    public void modificarProyecto(Proyecto proyecto);
    public void eliminarProyecto(Proyecto proyecto);
}
