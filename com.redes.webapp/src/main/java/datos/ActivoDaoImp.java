/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Activo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MarkII
 */
@Stateless
public class ActivoDaoImp implements ActivoDao{
    @PersistenceContext(unitName = "EvalEconPU")
    EntityManager em;
    
    @Override
    public List<Activo> listarActivos(){
        List<Activo> listadoActivos = em.createNamedQuery("Activo.findAll").getResultList();
        return listadoActivos;
    }

    @Override
    public Activo encontrarActivo(Activo activo) {
        return em.find(Activo.class, activo.getIdActivo());
    }

    @Override
    public void crearActivo(Activo activo) {
        em.persist(activo);
    }

    @Override
    public void modificarActivo(Activo activo) {
        em.merge(activo);
    }

    @Override
    public void eliminarActivo(Activo activo) {
        em.remove(em.merge(activo));
    }
    
}
