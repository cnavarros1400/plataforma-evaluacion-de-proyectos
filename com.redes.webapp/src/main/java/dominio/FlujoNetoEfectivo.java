/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "flujo_neto_efectivo")
@NamedQueries({
    @NamedQuery(name = "FlujoNetoEfectivo.findAll", query = "SELECT f FROM FlujoNetoEfectivo f"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByIdFlujos", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.idFlujos = :idFlujos"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByFnePrimerPer", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.fnePrimerPer = :fnePrimerPer"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByFneSegundoPer", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.fneSegundoPer = :fneSegundoPer"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByFneTercerPer", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.fneTercerPer = :fneTercerPer"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByFneCuartoPer", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.fneCuartoPer = :fneCuartoPer"),
    @NamedQuery(name = "FlujoNetoEfectivo.findByFneQuintoPer", query = "SELECT f FROM FlujoNetoEfectivo f WHERE f.fneQuintoPer = :fneQuintoPer")})
public class FlujoNetoEfectivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idFlujos;
    @Basic(optional = false)
    @NotNull
    private double fnePrimerPer;
    @Basic(optional = false)
    @NotNull
    private double fneSegundoPer;
    @Basic(optional = false)
    @NotNull
    private double fneTercerPer;
    @Basic(optional = false)
    @NotNull
    private double fneCuartoPer;
    @Basic(optional = false)
    @NotNull
    private double fneQuintoPer;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "flujoNetoEfectivo")
    private List<BalanceProforma> balanceProformaList;

    public FlujoNetoEfectivo() {
    }

    public FlujoNetoEfectivo(Integer idFlujos) {
        this.idFlujos = idFlujos;
    }

    public FlujoNetoEfectivo(Integer idFlujos, double fnePrimerPer, double fneSegundoPer, double fneTercerPer, double fneCuartoPer, double fneQuintoPer) {
        this.idFlujos = idFlujos;
        this.fnePrimerPer = fnePrimerPer;
        this.fneSegundoPer = fneSegundoPer;
        this.fneTercerPer = fneTercerPer;
        this.fneCuartoPer = fneCuartoPer;
        this.fneQuintoPer = fneQuintoPer;
    }

    public Integer getIdFlujos() {
        return idFlujos;
    }

    public void setIdFlujos(Integer idFlujos) {
        this.idFlujos = idFlujos;
    }

    public double getFnePrimerPer() {
        return fnePrimerPer;
    }

    public void setFnePrimerPer(double fnePrimerPer) {
        this.fnePrimerPer = fnePrimerPer;
    }

    public double getFneSegundoPer() {
        return fneSegundoPer;
    }

    public void setFneSegundoPer(double fneSegundoPer) {
        this.fneSegundoPer = fneSegundoPer;
    }

    public double getFneTercerPer() {
        return fneTercerPer;
    }

    public void setFneTercerPer(double fneTercerPer) {
        this.fneTercerPer = fneTercerPer;
    }

    public double getFneCuartoPer() {
        return fneCuartoPer;
    }

    public void setFneCuartoPer(double fneCuartoPer) {
        this.fneCuartoPer = fneCuartoPer;
    }

    public double getFneQuintoPer() {
        return fneQuintoPer;
    }

    public void setFneQuintoPer(double fneQuintoPer) {
        this.fneQuintoPer = fneQuintoPer;
    }

    public List<BalanceProforma> getBalanceProformaList() {
        return balanceProformaList;
    }

    public void setBalanceProformaList(List<BalanceProforma> balanceProformaList) {
        this.balanceProformaList = balanceProformaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFlujos != null ? idFlujos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FlujoNetoEfectivo)) {
            return false;
        }
        FlujoNetoEfectivo other = (FlujoNetoEfectivo) object;
        if ((this.idFlujos == null && other.idFlujos != null) || (this.idFlujos != null && !this.idFlujos.equals(other.idFlujos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.FlujoNetoEfectivo[ idFlujos=" + idFlujos + " ]";
    }
    
}
