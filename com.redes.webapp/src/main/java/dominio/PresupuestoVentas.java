/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "presupuesto_ventas")
@NamedQueries({
    @NamedQuery(name = "PresupuestoVentas.findAll", query = "SELECT p FROM PresupuestoVentas p"),
    @NamedQuery(name = "PresupuestoVentas.findByIdPresupuestoVentas", query = "SELECT p FROM PresupuestoVentas p WHERE p.idPresupuestoVentas = :idPresupuestoVentas"),
    @NamedQuery(name = "PresupuestoVentas.findByCostoPromedioPlato", query = "SELECT p FROM PresupuestoVentas p WHERE p.costoPromedioPlato = :costoPromedioPlato"),
    @NamedQuery(name = "PresupuestoVentas.findByPromedioPlatosDia", query = "SELECT p FROM PresupuestoVentas p WHERE p.promedioPlatosDia = :promedioPlatosDia"),
    @NamedQuery(name = "PresupuestoVentas.findByDiasVentaSemana", query = "SELECT p FROM PresupuestoVentas p WHERE p.diasVentaSemana = :diasVentaSemana"),
    @NamedQuery(name = "PresupuestoVentas.findByVentaMes", query = "SELECT p FROM PresupuestoVentas p WHERE p.ventaMes = :ventaMes"),
    @NamedQuery(name = "PresupuestoVentas.findByVentaAnual", query = "SELECT p FROM PresupuestoVentas p WHERE p.ventaAnual = :ventaAnual")})
public class PresupuestoVentas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPresupuesto_Ventas")
    private Integer idPresupuestoVentas;
    @Basic(optional = false)
    @NotNull
    private long costoPromedioPlato;
    @Basic(optional = false)
    @NotNull
    private int promedioPlatosDia;
    @Basic(optional = false)
    @NotNull
    private int diasVentaSemana;
    @Basic(optional = false)
    @NotNull
    private double ventaMes;
    @Basic(optional = false)
    @NotNull
    private double ventaAnual;
    @OneToMany(mappedBy = "presupuestoVentas1")
    private List<Proyecto> proyectoList;

    public PresupuestoVentas() {
    }

    public PresupuestoVentas(Integer idPresupuestoVentas) {
        this.idPresupuestoVentas = idPresupuestoVentas;
    }

    public PresupuestoVentas(Integer idPresupuestoVentas, long costoPromedioPlato, int promedioPlatosDia, int diasVentaSemana, double ventaMes, double ventaAnual) {
        this.idPresupuestoVentas = idPresupuestoVentas;
        this.costoPromedioPlato = costoPromedioPlato;
        this.promedioPlatosDia = promedioPlatosDia;
        this.diasVentaSemana = diasVentaSemana;
        this.ventaMes = ventaMes;
        this.ventaAnual = ventaAnual;
    }

    public Integer getIdPresupuestoVentas() {
        return idPresupuestoVentas;
    }

    public void setIdPresupuestoVentas(Integer idPresupuestoVentas) {
        this.idPresupuestoVentas = idPresupuestoVentas;
    }

    public long getCostoPromedioPlato() {
        return costoPromedioPlato;
    }

    public void setCostoPromedioPlato(long costoPromedioPlato) {
        this.costoPromedioPlato = costoPromedioPlato;
    }

    public int getPromedioPlatosDia() {
        return promedioPlatosDia;
    }

    public void setPromedioPlatosDia(int promedioPlatosDia) {
        this.promedioPlatosDia = promedioPlatosDia;
    }

    public int getDiasVentaSemana() {
        return diasVentaSemana;
    }

    public void setDiasVentaSemana(int diasVentaSemana) {
        this.diasVentaSemana = diasVentaSemana;
    }

    public double getVentaMes() {
        return ventaMes;
    }

    public void setVentaMes(double ventaMes) {
        this.ventaMes = ventaMes;
    }

    public double getVentaAnual() {
        return ventaAnual;
    }

    public void setVentaAnual(double ventaAnual) {
        this.ventaAnual = ventaAnual;
    }

    public List<Proyecto> getProyectoList() {
        return proyectoList;
    }

    public void setProyectoList(List<Proyecto> proyectoList) {
        this.proyectoList = proyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPresupuestoVentas != null ? idPresupuestoVentas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PresupuestoVentas)) {
            return false;
        }
        PresupuestoVentas other = (PresupuestoVentas) object;
        if ((this.idPresupuestoVentas == null && other.idPresupuestoVentas != null) || (this.idPresupuestoVentas != null && !this.idPresupuestoVentas.equals(other.idPresupuestoVentas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.PresupuestoVentas[ idPresupuestoVentas=" + idPresupuestoVentas + " ]";
    }
    
}
