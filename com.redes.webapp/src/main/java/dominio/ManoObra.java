/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "mano_obra")
@NamedQueries({
    @NamedQuery(name = "ManoObra.findAll", query = "SELECT m FROM ManoObra m"),
    @NamedQuery(name = "ManoObra.findByIdManoObra", query = "SELECT m FROM ManoObra m WHERE m.idManoObra = :idManoObra"),
    @NamedQuery(name = "ManoObra.findByCargo", query = "SELECT m FROM ManoObra m WHERE m.cargo = :cargo"),
    @NamedQuery(name = "ManoObra.findByAreaManoObra", query = "SELECT m FROM ManoObra m WHERE m.areaManoObra = :areaManoObra"),
    @NamedQuery(name = "ManoObra.findByCantidadMO", query = "SELECT m FROM ManoObra m WHERE m.cantidadMO = :cantidadMO"),
    @NamedQuery(name = "ManoObra.findByCostoDiarioMO", query = "SELECT m FROM ManoObra m WHERE m.costoDiarioMO = :costoDiarioMO"),
    @NamedQuery(name = "ManoObra.findByCostoMensualMO", query = "SELECT m FROM ManoObra m WHERE m.costoMensualMO = :costoMensualMO"),
    @NamedQuery(name = "ManoObra.findByCostoAnualMO", query = "SELECT m FROM ManoObra m WHERE m.costoAnualMO = :costoAnualMO")})
public class ManoObra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMano_Obra")
    private Integer idManoObra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String cargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String areaManoObra;
    @Basic(optional = false)
    @NotNull
    private int cantidadMO;
    @Basic(optional = false)
    @NotNull
    private double costoDiarioMO;
    @Basic(optional = false)
    @NotNull
    private double costoMensualMO;
    @Basic(optional = false)
    @NotNull
    private double costoAnualMO;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public ManoObra() {
    }

    public ManoObra(Integer idManoObra) {
        this.idManoObra = idManoObra;
    }

    public ManoObra(Integer idManoObra, String cargo, String areaManoObra, int cantidadMO, double costoDiarioMO, double costoMensualMO, double costoAnualMO) {
        this.idManoObra = idManoObra;
        this.cargo = cargo;
        this.areaManoObra = areaManoObra;
        this.cantidadMO = cantidadMO;
        this.costoDiarioMO = costoDiarioMO;
        this.costoMensualMO = costoMensualMO;
        this.costoAnualMO = costoAnualMO;
    }

    public Integer getIdManoObra() {
        return idManoObra;
    }

    public void setIdManoObra(Integer idManoObra) {
        this.idManoObra = idManoObra;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getAreaManoObra() {
        return areaManoObra;
    }

    public void setAreaManoObra(String areaManoObra) {
        this.areaManoObra = areaManoObra;
    }

    public int getCantidadMO() {
        return cantidadMO;
    }

    public void setCantidadMO(int cantidadMO) {
        this.cantidadMO = cantidadMO;
    }

    public double getCostoDiarioMO() {
        return costoDiarioMO;
    }

    public void setCostoDiarioMO(double costoDiarioMO) {
        this.costoDiarioMO = costoDiarioMO;
    }

    public double getCostoMensualMO() {
        return costoMensualMO;
    }

    public void setCostoMensualMO(double costoMensualMO) {
        this.costoMensualMO = costoMensualMO;
    }

    public double getCostoAnualMO() {
        return costoAnualMO;
    }

    public void setCostoAnualMO(double costoAnualMO) {
        this.costoAnualMO = costoAnualMO;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idManoObra != null ? idManoObra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ManoObra)) {
            return false;
        }
        ManoObra other = (ManoObra) object;
        if ((this.idManoObra == null && other.idManoObra != null) || (this.idManoObra != null && !this.idManoObra.equals(other.idManoObra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.ManoObra[ idManoObra=" + idManoObra + " ]";
    }
    
}
