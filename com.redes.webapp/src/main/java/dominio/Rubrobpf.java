/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Rubrobpf.findAll", query = "SELECT r FROM Rubrobpf r"),
    @NamedQuery(name = "Rubrobpf.findByIdRubroBPF", query = "SELECT r FROM Rubrobpf r WHERE r.idRubroBPF = :idRubroBPF"),
    @NamedQuery(name = "Rubrobpf.findByRubro", query = "SELECT r FROM Rubrobpf r WHERE r.rubro = :rubro")})
public class Rubrobpf implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idRubroBPF;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String rubro;
    @ManyToMany(mappedBy = "rubrobpfList")
    private List<BalanceProforma> balanceProformaList;

    public Rubrobpf() {
    }

    public Rubrobpf(Integer idRubroBPF) {
        this.idRubroBPF = idRubroBPF;
    }

    public Rubrobpf(Integer idRubroBPF, String rubro) {
        this.idRubroBPF = idRubroBPF;
        this.rubro = rubro;
    }

    public Integer getIdRubroBPF() {
        return idRubroBPF;
    }

    public void setIdRubroBPF(Integer idRubroBPF) {
        this.idRubroBPF = idRubroBPF;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public List<BalanceProforma> getBalanceProformaList() {
        return balanceProformaList;
    }

    public void setBalanceProformaList(List<BalanceProforma> balanceProformaList) {
        this.balanceProformaList = balanceProformaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRubroBPF != null ? idRubroBPF.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rubrobpf)) {
            return false;
        }
        Rubrobpf other = (Rubrobpf) object;
        if ((this.idRubroBPF == null && other.idRubroBPF != null) || (this.idRubroBPF != null && !this.idRubroBPF.equals(other.idRubroBPF))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Rubrobpf[ idRubroBPF=" + idRubroBPF + " ]";
    }
    
}
