/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Servicios.findAll", query = "SELECT s FROM Servicios s"),
    @NamedQuery(name = "Servicios.findByIdCostoServicios", query = "SELECT s FROM Servicios s WHERE s.idCostoServicios = :idCostoServicios"),
    @NamedQuery(name = "Servicios.findByServicio", query = "SELECT s FROM Servicios s WHERE s.servicio = :servicio"),
    @NamedQuery(name = "Servicios.findByConsumoMensualServ", query = "SELECT s FROM Servicios s WHERE s.consumoMensualServ = :consumoMensualServ"),
    @NamedQuery(name = "Servicios.findByConsumoAnualServ", query = "SELECT s FROM Servicios s WHERE s.consumoAnualServ = :consumoAnualServ")})
public class Servicios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCosto_Servicios")
    private Integer idCostoServicios;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String servicio;
    @Basic(optional = false)
    @NotNull
    private double consumoMensualServ;
    @Basic(optional = false)
    @NotNull
    private double consumoAnualServ;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public Servicios() {
    }

    public Servicios(Integer idCostoServicios) {
        this.idCostoServicios = idCostoServicios;
    }

    public Servicios(Integer idCostoServicios, String servicio, double consumoMensualServ, double consumoAnualServ) {
        this.idCostoServicios = idCostoServicios;
        this.servicio = servicio;
        this.consumoMensualServ = consumoMensualServ;
        this.consumoAnualServ = consumoAnualServ;
    }

    public Integer getIdCostoServicios() {
        return idCostoServicios;
    }

    public void setIdCostoServicios(Integer idCostoServicios) {
        this.idCostoServicios = idCostoServicios;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public double getConsumoMensualServ() {
        return consumoMensualServ;
    }

    public void setConsumoMensualServ(double consumoMensualServ) {
        this.consumoMensualServ = consumoMensualServ;
    }

    public double getConsumoAnualServ() {
        return consumoAnualServ;
    }

    public void setConsumoAnualServ(double consumoAnualServ) {
        this.consumoAnualServ = consumoAnualServ;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCostoServicios != null ? idCostoServicios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicios)) {
            return false;
        }
        Servicios other = (Servicios) object;
        if ((this.idCostoServicios == null && other.idCostoServicios != null) || (this.idCostoServicios != null && !this.idCostoServicios.equals(other.idCostoServicios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Servicios[ idCostoServicios=" + idCostoServicios + " ]";
    }
    
}
