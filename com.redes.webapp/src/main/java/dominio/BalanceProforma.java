/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "balance_proforma")
@NamedQueries({
    @NamedQuery(name = "BalanceProforma.findAll", query = "SELECT b FROM BalanceProforma b"),
    @NamedQuery(name = "BalanceProforma.findByIdBalanceProForma", query = "SELECT b FROM BalanceProforma b WHERE b.idBalanceProForma = :idBalanceProForma")})
public class BalanceProforma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idBalance_ProForma")
    private Integer idBalanceProForma;
    @JoinTable(name = "balance_proforma_has_rubrobpf", joinColumns = {
        @JoinColumn(name = "Balance_ProForma_idBalance_ProForma", referencedColumnName = "idBalance_ProForma")}, inverseJoinColumns = {
        @JoinColumn(name = "RubroBPF_idRubroBPF", referencedColumnName = "idRubroBPF")})
    @ManyToMany
    private List<Rubrobpf> rubrobpfList;
    @JoinColumn(name = "Flujo_Neto_Efectivo_idFlujos", referencedColumnName = "idFlujos")
    @ManyToOne(optional = false)
    private FlujoNetoEfectivo flujoNetoEfectivo;
    @OneToMany(mappedBy = "balanceProforma")
    private List<Proyecto> proyectoList;

    public BalanceProforma() {
    }

    public BalanceProforma(Integer idBalanceProForma) {
        this.idBalanceProForma = idBalanceProForma;
    }

    public Integer getIdBalanceProForma() {
        return idBalanceProForma;
    }

    public void setIdBalanceProForma(Integer idBalanceProForma) {
        this.idBalanceProForma = idBalanceProForma;
    }

    public List<Rubrobpf> getRubrobpfList() {
        return rubrobpfList;
    }

    public void setRubrobpfList(List<Rubrobpf> rubrobpfList) {
        this.rubrobpfList = rubrobpfList;
    }

    public FlujoNetoEfectivo getFlujoNetoEfectivo() {
        return flujoNetoEfectivo;
    }

    public void setFlujoNetoEfectivo(FlujoNetoEfectivo flujoNetoEfectivo) {
        this.flujoNetoEfectivo = flujoNetoEfectivo;
    }

    public List<Proyecto> getProyectoList() {
        return proyectoList;
    }

    public void setProyectoList(List<Proyecto> proyectoList) {
        this.proyectoList = proyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBalanceProForma != null ? idBalanceProForma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BalanceProforma)) {
            return false;
        }
        BalanceProforma other = (BalanceProforma) object;
        if ((this.idBalanceProForma == null && other.idBalanceProForma != null) || (this.idBalanceProForma != null && !this.idBalanceProForma.equals(other.idBalanceProForma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.BalanceProforma[ idBalanceProForma=" + idBalanceProForma + " ]";
    }
    
}
