/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Indicadores.findAll", query = "SELECT i FROM Indicadores i"),
    @NamedQuery(name = "Indicadores.findByIdIndicadore", query = "SELECT i FROM Indicadores i WHERE i.idIndicadore = :idIndicadore"),
    @NamedQuery(name = "Indicadores.findByNombreIndicador", query = "SELECT i FROM Indicadores i WHERE i.nombreIndicador = :nombreIndicador"),
    @NamedQuery(name = "Indicadores.findByValorIndicador", query = "SELECT i FROM Indicadores i WHERE i.valorIndicador = :valorIndicador")})
public class Indicadores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idIndicadore;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String nombreIndicador;
    @Basic(optional = false)
    @NotNull
    private double valorIndicador;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public Indicadores() {
    }

    public Indicadores(Integer idIndicadore) {
        this.idIndicadore = idIndicadore;
    }

    public Indicadores(Integer idIndicadore, String nombreIndicador, double valorIndicador) {
        this.idIndicadore = idIndicadore;
        this.nombreIndicador = nombreIndicador;
        this.valorIndicador = valorIndicador;
    }

    public Integer getIdIndicadore() {
        return idIndicadore;
    }

    public void setIdIndicadore(Integer idIndicadore) {
        this.idIndicadore = idIndicadore;
    }

    public String getNombreIndicador() {
        return nombreIndicador;
    }

    public void setNombreIndicador(String nombreIndicador) {
        this.nombreIndicador = nombreIndicador;
    }

    public double getValorIndicador() {
        return valorIndicador;
    }

    public void setValorIndicador(double valorIndicador) {
        this.valorIndicador = valorIndicador;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIndicadore != null ? idIndicadore.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Indicadores)) {
            return false;
        }
        Indicadores other = (Indicadores) object;
        if ((this.idIndicadore == null && other.idIndicadore != null) || (this.idIndicadore != null && !this.idIndicadore.equals(other.idIndicadore))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Indicadores[ idIndicadore=" + idIndicadore + " ]";
    }
    
}
