/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Activo.findAll", query = "SELECT a FROM Activo a"),
    @NamedQuery(name = "Activo.findByIdActivo", query = "SELECT a FROM Activo a WHERE a.idActivo = :idActivo"),
    @NamedQuery(name = "Activo.findByNombreActivo", query = "SELECT a FROM Activo a WHERE a.nombreActivo = :nombreActivo"),
    @NamedQuery(name = "Activo.findByAreaUbicacion", query = "SELECT a FROM Activo a WHERE a.areaUbicacion = :areaUbicacion"),
    @NamedQuery(name = "Activo.findByTipoActivo", query = "SELECT a FROM Activo a WHERE a.tipoActivo = :tipoActivo"),
    @NamedQuery(name = "Activo.findByCantidadActivo", query = "SELECT a FROM Activo a WHERE a.cantidadActivo = :cantidadActivo"),
    @NamedQuery(name = "Activo.findByValorUnitarioAct", query = "SELECT a FROM Activo a WHERE a.valorUnitarioAct = :valorUnitarioAct"),
    @NamedQuery(name = "Activo.findByValorTotalAct", query = "SELECT a FROM Activo a WHERE a.valorTotalAct = :valorTotalAct")})
public class Activo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idActivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String nombreActivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String areaUbicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String tipoActivo;
    @Basic(optional = false)
    @NotNull
    private int cantidadActivo;
    @Basic(optional = false)
    @NotNull
    private double valorUnitarioAct;
    @Basic(optional = false)
    @NotNull
    private double valorTotalAct;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public Activo() {
    }

    public Activo(Integer idActivo) {
        this.idActivo = idActivo;
    }

    public Activo(String nombreActivo, String areaUbicacion, String tipoActivo, int cantidadActivo, double valorUnitarioAct, double valorTotalAct, Proyecto proy) {
        this.nombreActivo = nombreActivo;
        this.areaUbicacion = areaUbicacion;
        this.tipoActivo = tipoActivo;
        this.cantidadActivo = cantidadActivo;
        this.valorUnitarioAct = valorUnitarioAct;
        this.valorTotalAct = valorTotalAct;
        this.proyecto = proy;
    }

    public Integer getIdActivo() {
        return idActivo;
    }

    public void setIdActivo(Integer idActivo) {
        this.idActivo = idActivo;
    }

    public String getNombreActivo() {
        return nombreActivo;
    }

    public void setNombreActivo(String nombreActivo) {
        this.nombreActivo = nombreActivo;
    }

    public String getAreaUbicacion() {
        return areaUbicacion;
    }

    public void setAreaUbicacion(String areaUbicacion) {
        this.areaUbicacion = areaUbicacion;
    }

    public String getTipoActivo() {
        return tipoActivo;
    }

    public void setTipoActivo(String tipoActivo) {
        this.tipoActivo = tipoActivo;
    }

    public int getCantidadActivo() {
        return cantidadActivo;
    }

    public void setCantidadActivo(int cantidadActivo) {
        this.cantidadActivo = cantidadActivo;
    }

    public double getValorUnitarioAct() {
        return valorUnitarioAct;
    }

    public void setValorUnitarioAct(double valorUnitarioAct) {
        this.valorUnitarioAct = valorUnitarioAct;
    }

    public double getValorTotalAct() {
        return valorTotalAct;
    }

    public void setValorTotalAct(double valorTotalAct) {
        this.valorTotalAct = valorTotalAct;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActivo != null ? idActivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activo)) {
            return false;
        }
        Activo other = (Activo) object;
        if ((this.idActivo == null && other.idActivo != null) || (this.idActivo != null && !this.idActivo.equals(other.idActivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Activo[ idActivo=" + idActivo + " ]";
    }
    
}
