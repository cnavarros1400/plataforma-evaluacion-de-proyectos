/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Insumos.findAll", query = "SELECT i FROM Insumos i"),
    @NamedQuery(name = "Insumos.findByIdInsumos", query = "SELECT i FROM Insumos i WHERE i.idInsumos = :idInsumos"),
    @NamedQuery(name = "Insumos.findByNombreInsumo", query = "SELECT i FROM Insumos i WHERE i.nombreInsumo = :nombreInsumo"),
    @NamedQuery(name = "Insumos.findByCantidadInsumo", query = "SELECT i FROM Insumos i WHERE i.cantidadInsumo = :cantidadInsumo"),
    @NamedQuery(name = "Insumos.findByCostoUnitarioIns", query = "SELECT i FROM Insumos i WHERE i.costoUnitarioIns = :costoUnitarioIns"),
    @NamedQuery(name = "Insumos.findByCostoMensualInsumo", query = "SELECT i FROM Insumos i WHERE i.costoMensualInsumo = :costoMensualInsumo"),
    @NamedQuery(name = "Insumos.findByCostoAnualInsumo", query = "SELECT i FROM Insumos i WHERE i.costoAnualInsumo = :costoAnualInsumo")})
public class Insumos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idInsumos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String nombreInsumo;
    @Basic(optional = false)
    @NotNull
    private int cantidadInsumo;
    @Basic(optional = false)
    @NotNull
    private double costoUnitarioIns;
    @Basic(optional = false)
    @NotNull
    private double costoMensualInsumo;
    @Basic(optional = false)
    @NotNull
    private double costoAnualInsumo;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public Insumos() {
    }

    public Insumos(Integer idInsumos) {
        this.idInsumos = idInsumos;
    }

    public Insumos(Integer idInsumos, String nombreInsumo, int cantidadInsumo, double costoUnitarioIns, double costoMensualInsumo, double costoAnualInsumo) {
        this.idInsumos = idInsumos;
        this.nombreInsumo = nombreInsumo;
        this.cantidadInsumo = cantidadInsumo;
        this.costoUnitarioIns = costoUnitarioIns;
        this.costoMensualInsumo = costoMensualInsumo;
        this.costoAnualInsumo = costoAnualInsumo;
    }

    public Integer getIdInsumos() {
        return idInsumos;
    }

    public void setIdInsumos(Integer idInsumos) {
        this.idInsumos = idInsumos;
    }

    public String getNombreInsumo() {
        return nombreInsumo;
    }

    public void setNombreInsumo(String nombreInsumo) {
        this.nombreInsumo = nombreInsumo;
    }

    public int getCantidadInsumo() {
        return cantidadInsumo;
    }

    public void setCantidadInsumo(int cantidadInsumo) {
        this.cantidadInsumo = cantidadInsumo;
    }

    public double getCostoUnitarioIns() {
        return costoUnitarioIns;
    }

    public void setCostoUnitarioIns(double costoUnitarioIns) {
        this.costoUnitarioIns = costoUnitarioIns;
    }

    public double getCostoMensualInsumo() {
        return costoMensualInsumo;
    }

    public void setCostoMensualInsumo(double costoMensualInsumo) {
        this.costoMensualInsumo = costoMensualInsumo;
    }

    public double getCostoAnualInsumo() {
        return costoAnualInsumo;
    }

    public void setCostoAnualInsumo(double costoAnualInsumo) {
        this.costoAnualInsumo = costoAnualInsumo;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInsumos != null ? idInsumos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Insumos)) {
            return false;
        }
        Insumos other = (Insumos) object;
        if ((this.idInsumos == null && other.idInsumos != null) || (this.idInsumos != null && !this.idInsumos.equals(other.idInsumos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Insumos[ idInsumos=" + idInsumos + " ]";
    }
    
}
