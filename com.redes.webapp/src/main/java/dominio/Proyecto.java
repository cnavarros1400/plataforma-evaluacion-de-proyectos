/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p"),
    @NamedQuery(name = "Proyecto.findByIdProyecto", query = "SELECT p FROM Proyecto p WHERE p.idProyecto = :idProyecto"),
    @NamedQuery(name = "Proyecto.findByNombreProyecto", query = "SELECT p FROM Proyecto p WHERE p.nombreProyecto = :nombreProyecto"),
    @NamedQuery(name = "Proyecto.findByFecha", query = "SELECT p FROM Proyecto p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Proyecto.findByInversionInicial", query = "SELECT p FROM Proyecto p WHERE p.inversionInicial = :inversionInicial"),
    @NamedQuery(name = "Proyecto.findByPresupuestoVentas", query = "SELECT p FROM Proyecto p WHERE p.presupuestoVentas = :presupuestoVentas"),
    @NamedQuery(name = "Proyecto.findByPresupuestoCostos", query = "SELECT p FROM Proyecto p WHERE p.presupuestoCostos = :presupuestoCostos"),
    @NamedQuery(name = "Proyecto.findByFlujoNetoEfectivoInicial", query = "SELECT p FROM Proyecto p WHERE p.flujoNetoEfectivoInicial = :flujoNetoEfectivoInicial"),
    @NamedQuery(name = "Proyecto.findByProyectoViable", query = "SELECT p FROM Proyecto p WHERE p.proyectoViable = :proyectoViable")})
public class Proyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer idProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String nombreProyecto;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date fecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private Double inversionInicial;
    private Double presupuestoVentas;
    private Double presupuestoCostos;
    private Double flujoNetoEfectivoInicial;
    private Integer proyectoViable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<MateriaPrima> materiaPrimaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<Servicios> serviciosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<Insumos> insumosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<ManoObra> manoObraList;
    @JoinColumn(name = "Balance_ProForma_idBalance_ProForma", referencedColumnName = "idBalance_ProForma")
    @ManyToOne
    private BalanceProforma balanceProforma;
    @JoinColumn(name = "Presupuesto_Ventas_idPresupuesto_Ventas", referencedColumnName = "idPresupuesto_Ventas")
    @ManyToOne
    private PresupuestoVentas presupuestoVentas1;
    //@JoinColumn(name = "Usuario_idUsuario", referencedColumnName = "idUsuario")
    @ManyToOne
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<Indicadores> indicadoresList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private List<Activo> activoList;

    public Proyecto() {
    }

    public Proyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Proyecto(Usuario usu, String nombreProyecto, Date fecha) {
        this.usuario = usu;
        this.nombreProyecto = nombreProyecto;
        this.fecha = fecha;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getInversionInicial() {
        return inversionInicial;
    }

    public void setInversionInicial(Double inversionInicial) {
        this.inversionInicial = inversionInicial;
    }

    public Double getPresupuestoVentas() {
        return presupuestoVentas;
    }

    public void setPresupuestoVentas(Double presupuestoVentas) {
        this.presupuestoVentas = presupuestoVentas;
    }

    public Double getPresupuestoCostos() {
        return presupuestoCostos;
    }

    public void setPresupuestoCostos(Double presupuestoCostos) {
        this.presupuestoCostos = presupuestoCostos;
    }

    public Double getFlujoNetoEfectivoInicial() {
        return flujoNetoEfectivoInicial;
    }

    public void setFlujoNetoEfectivoInicial(Double flujoNetoEfectivoInicial) {
        this.flujoNetoEfectivoInicial = flujoNetoEfectivoInicial;
    }

    public Integer getProyectoViable() {
        return proyectoViable;
    }

    public void setProyectoViable(Integer proyectoViable) {
        this.proyectoViable = proyectoViable;
    }

    public List<MateriaPrima> getMateriaPrimaList() {
        return materiaPrimaList;
    }

    public void setMateriaPrimaList(List<MateriaPrima> materiaPrimaList) {
        this.materiaPrimaList = materiaPrimaList;
    }

    public List<Servicios> getServiciosList() {
        return serviciosList;
    }

    public void setServiciosList(List<Servicios> serviciosList) {
        this.serviciosList = serviciosList;
    }

    public List<Insumos> getInsumosList() {
        return insumosList;
    }

    public void setInsumosList(List<Insumos> insumosList) {
        this.insumosList = insumosList;
    }

    public List<ManoObra> getManoObraList() {
        return manoObraList;
    }

    public void setManoObraList(List<ManoObra> manoObraList) {
        this.manoObraList = manoObraList;
    }

    public BalanceProforma getBalanceProforma() {
        return balanceProforma;
    }

    public void setBalanceProforma(BalanceProforma balanceProforma) {
        this.balanceProforma = balanceProforma;
    }

    public PresupuestoVentas getPresupuestoVentas1() {
        return presupuestoVentas1;
    }

    public void setPresupuestoVentas1(PresupuestoVentas presupuestoVentas1) {
        this.presupuestoVentas1 = presupuestoVentas1;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Indicadores> getIndicadoresList() {
        return indicadoresList;
    }

    public void setIndicadoresList(List<Indicadores> indicadoresList) {
        this.indicadoresList = indicadoresList;
    }

    public List<Activo> getActivoList() {
        return activoList;
    }

    public void setActivoList(List<Activo> activoList) {
        this.activoList = activoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyecto != null ? idProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idProyecto == null && other.idProyecto != null) || (this.idProyecto != null && !this.idProyecto.equals(other.idProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Proyecto[ idProyecto=" + idProyecto + " Nombre Proyecto. " + nombreProyecto + " ]";
    }
    
}
