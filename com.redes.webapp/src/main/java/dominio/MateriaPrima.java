/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "materia_prima")
@NamedQueries({
    @NamedQuery(name = "MateriaPrima.findAll", query = "SELECT m FROM MateriaPrima m"),
    @NamedQuery(name = "MateriaPrima.findByIdMateriaPrima", query = "SELECT m FROM MateriaPrima m WHERE m.idMateriaPrima = :idMateriaPrima"),
    @NamedQuery(name = "MateriaPrima.findByNombreMP", query = "SELECT m FROM MateriaPrima m WHERE m.nombreMP = :nombreMP"),
    @NamedQuery(name = "MateriaPrima.findByUnidadMedida", query = "SELECT m FROM MateriaPrima m WHERE m.unidadMedida = :unidadMedida"),
    @NamedQuery(name = "MateriaPrima.findByCantidad", query = "SELECT m FROM MateriaPrima m WHERE m.cantidad = :cantidad"),
    @NamedQuery(name = "MateriaPrima.findByCostoUnitarioMP", query = "SELECT m FROM MateriaPrima m WHERE m.costoUnitarioMP = :costoUnitarioMP"),
    @NamedQuery(name = "MateriaPrima.findByCostoMensualMP", query = "SELECT m FROM MateriaPrima m WHERE m.costoMensualMP = :costoMensualMP"),
    @NamedQuery(name = "MateriaPrima.findByCostoAnualMP", query = "SELECT m FROM MateriaPrima m WHERE m.costoAnualMP = :costoAnualMP")})
public class MateriaPrima implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMateria_Prima")
    private Integer idMateriaPrima;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String nombreMP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String unidadMedida;
    @Basic(optional = false)
    @NotNull
    private long cantidad;
    @Basic(optional = false)
    @NotNull
    private double costoUnitarioMP;
    @Basic(optional = false)
    @NotNull
    private double costoMensualMP;
    @Basic(optional = false)
    @NotNull
    private double costoAnualMP;
    @JoinColumn(name = "Proyecto_idProyecto", referencedColumnName = "idProyecto")
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public MateriaPrima() {
    }

    public MateriaPrima(Integer idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    public MateriaPrima(Integer idMateriaPrima, String nombreMP, String unidadMedida, long cantidad, double costoUnitarioMP, double costoMensualMP, double costoAnualMP) {
        this.idMateriaPrima = idMateriaPrima;
        this.nombreMP = nombreMP;
        this.unidadMedida = unidadMedida;
        this.cantidad = cantidad;
        this.costoUnitarioMP = costoUnitarioMP;
        this.costoMensualMP = costoMensualMP;
        this.costoAnualMP = costoAnualMP;
    }

    public Integer getIdMateriaPrima() {
        return idMateriaPrima;
    }

    public void setIdMateriaPrima(Integer idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    public String getNombreMP() {
        return nombreMP;
    }

    public void setNombreMP(String nombreMP) {
        this.nombreMP = nombreMP;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public double getCostoUnitarioMP() {
        return costoUnitarioMP;
    }

    public void setCostoUnitarioMP(double costoUnitarioMP) {
        this.costoUnitarioMP = costoUnitarioMP;
    }

    public double getCostoMensualMP() {
        return costoMensualMP;
    }

    public void setCostoMensualMP(double costoMensualMP) {
        this.costoMensualMP = costoMensualMP;
    }

    public double getCostoAnualMP() {
        return costoAnualMP;
    }

    public void setCostoAnualMP(double costoAnualMP) {
        this.costoAnualMP = costoAnualMP;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMateriaPrima != null ? idMateriaPrima.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MateriaPrima)) {
            return false;
        }
        MateriaPrima other = (MateriaPrima) object;
        if ((this.idMateriaPrima == null && other.idMateriaPrima != null) || (this.idMateriaPrima != null && !this.idMateriaPrima.equals(other.idMateriaPrima))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.MateriaPrima[ idMateriaPrima=" + idMateriaPrima + " ]";
    }
    
}
